# README #

This README will document whatever steps are necessary to get your application up and running.

### This project use ###

* Python 3
* NumPy
* matplotlib
* Images provided by the teacher in .npy format

### How do I get set up? ###

* pip3 install matplotlib
* pip3 install scikit-learn

### How to run the project? ###

* ./src/aa.py
