#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys # check version
import math # basic maths
import numpy as np # matrix manipulation
import matplotlib.pyplot as plt # images manipulation
from sklearn.decomposition import PCA # PCA reduction
import timeit # get execution time
from sklearn.pipeline import make_pipeline # SVM
from sklearn.preprocessing import StandardScaler # SVM
from sklearn import svm # SVM


import os.path
from os import path


if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.");


def openfile():
    if(path.exists(os.path.dirname(__file__)+"/../data/dev_img.npy")):
        print("loading data")
        return [np.load(os.path.dirname(__file__)+"/../data/trn_img.npy"),
                np.load(os.path.dirname(__file__)+"/../data/trn_lbl.npy"),
                np.load(os.path.dirname(__file__)+"/../data/dev_img.npy"),
                np.load(os.path.dirname(__file__)+"/../data/dev_lbl.npy"),
                np.load(os.path.dirname(__file__)+"/../data/tst_img.npy")]
    else:
        print("data not found (expected to be located on",os.path.dirname(__file__),"/../data/[trn|dev|tst]_[img|lbl].npy")
        exit -1

# disply an image
# usage: disp(X_trn[3])
def disp(img):
    imgg = img.reshape(32,24)
    plt.imshow(imgg, plt.cm.gray)
    plt.show()

# 1
# Implémentation du classifieur Bayésien gaussien :
# chaque classe est représentée par la moyenne des vecteurs d’apprentissage de
# cette classe et leur matrice de covariance. Vous calculerez la performance sur
# l’ensemble de développement grâce au taux d’erreur défini comme le nombre
# d’exemples mal classés divisé par le nombre total d’exemples.
# gaussClf will be the class that will have the Gaussian naive bayes classifier implimentation

class gaussClf:
    #divise le jeu de données dans des classes distinctes
    def separate_by_classes(self, X, y):
        #prepare les données exploitées/à remplir dans les bons formats
        self.classes = np.unique(y) #unique retourne les données classées sans doubons
        classes_index = {}
        subdatasets = {}
        cls, counts = np.unique(y, return_counts=True)
        self.class_freq = dict(zip(cls, counts)) #zip concat_èen des données en
        #tuple et dict en fait une table indexée
        #print("bayes_classifier: \t\tclasses: ",self.class_freq)
        for class_type in self.classes: #pour chaque classe, on calcule la fréquence et on
            classes_index[class_type] = np.argwhere(y==class_type)
            subdatasets[class_type] = X[classes_index[class_type], :]
            self.class_freq[class_type] = self.class_freq[class_type]/sum(list(self.class_freq.values()))
        return subdatasets

    # on calcule les moyennes des vecteurs (ici la taille des vecteurs
    # influe sur le temps de traitement de manière significative)
    def fit(self, X, y):
        separated_X = self.separate_by_classes(X, y)
        self.means = {}
        self.std = {}
        for class_type in self.classes:
            self.means[class_type] = np.mean(separated_X[class_type], axis=0)[0]
            self.std[class_type] = np.std(separated_X[class_type], axis=0)[0]

    #see predict_proba
    def calculate_probability(self, x, mean, stdev):
        exponent = math.exp(-((x - mean) ** 2 / (2 * stdev ** 2)))
        return (1 / (math.sqrt(2 * math.pi) * stdev)) * exponent

    # on utilise la distribution de gauss pour déduire la répartition entre
    # les classes
    def predict_proba(self, X):
        self.class_prob = {cls:math.log(self.class_freq[cls], math.e) for cls in self.classes}
        for cls in self.classes:
            for i in range(len(self.means)):
                self.class_prob[cls]+=math.log(self.calculate_probability(X[i], self.means[cls][i], self.std[cls][i]), math.e)
        self.class_prob = {cls: math.e**self.class_prob[cls] for cls in self.class_prob}
        return self.class_prob

    # déduit la classe d'une donnée en utilisant la répartition de gauss
    def predict(self, X):
        pred = []
        for x in X:
            pred_class = None
            max_prob = 0
            for cls, prob in self.predict_proba(x).items():
                if prob>max_prob:
                    max_prob = prob
                    pred_class = cls
            pred.append(pred_class)
        return pred

    # calcul du taux d'érreur
    def compute_error(self, prediction, Y_dev, traces):
        err = 0
        for i in range(len(prediction)):
            if(prediction[i]!=Y_dev[i]):
                ##print(prediction[i], "__", Y_dev[i])
                err += 1
        self.er = err/len(prediction)
        if(traces): print("\t\t\terror rate = ",round(self.er,3))
        return self.er

#use the gaussClf and disp result
def bayes_classifier(X_trn, Y_trn, X_dev, Y_dev, traces):
    start = timeit.default_timer()
    if(traces): print("\nQuestion 1 : Bayes Classifier")
    if(traces): print("bayes_classifier: start...")
    classifier = gaussClf()
    if(traces): print("bayes_classifier: \ttraining...")
    classifier.fit(X_trn, Y_trn)
    if(traces): print("bayes_classifier: \tpredicting...")
    prediction = classifier.predict(X_dev)
    if(traces): print("bayes_classifier: \tcomputing error rate...")
    error_rate = classifier.compute_error(prediction, Y_dev, traces);
    if(traces): print("bayes_classifier: finished")
    if(traces): print(round(timeit.default_timer()-start,3), "seconds")
    return [classifier,(timeit.default_timer()-start)]

# 2
# Réduction de la dimension des vecteurs : l’analyse en composantes principales
# (ACP) permet de réduire la dimension des vecteurs d’images à un vecteur de
# paramètres de plus petite taille (Pour Python, vous pouvez utiliserer la
# librairie Scikit-Learn sklearn.decomposition.PCA ). Vous testerez l’impact de
# la réduction de dimension sur le classifieur précédent (ACP+Gaussien) et vous
# tracerez la performance en fonction de la dimension de l’ACP sur une dizaine
# de valeurs représentatives.

def acp_bayes_classifier(X_trn, Y_trn, X_dev, Y_dev):
    print("\n\nQuestion 2 : PCA Bayes Classifier")
    print("nb_component","\t","error_rate","\t","computing_time")
    # values must be between 0 and min(n_samples, n_features)=768
    size_values = [768, 600, 500, 400, 300, 200, 100, 50, 10]
    for size in size_values:
        reductor = PCA(n_components=size)
        reduced_X_trn = reductor.fit_transform(X_trn)
        reduced_X_dev = reductor.transform(X_dev)
        bc = bayes_classifier(reduced_X_trn, Y_trn, reduced_X_dev, Y_dev, False)
        print(len(reduced_X_trn[0]),"\t\t", round(bc[0].er,3),"\t\t", round(bc[1],3),"s")

# 3
# Comparaison avec d’autres classifieurs, par exemple les Support Vector
# Machines ( sklearn.svm.SVC en Python) ou les plus proches voisins
# (sklearn.neighbors.KNeighborsClassifier en Python).

def svm_compute_error(prediction, Y_dev):
    err = 0
    for i in range(len(prediction)):
        if(prediction[i]!=Y_dev[i]):
            err += 1
    err = err/len(prediction)
    return err

def svm_classifier(X_trn, Y_trn, X_dev, Y_dev):
    print("\n\nQuestion 3 : Support Vector Machines")
    start = timeit.default_timer()
    print("svm_classifier: start...")
    classifier = make_pipeline(StandardScaler(), svm.SVC(gamma='auto'))
    print("svm_classifier: \ttraining...")
    classifier.fit(X_trn, Y_trn)
    print("svm_classifier: \tpredicting...")
    prediction = classifier.predict(X_dev)
    print("svm_classifier: \tcomputing error rate...")
    error_rate = svm_compute_error(prediction, Y_dev);
    print("\t\t\terror rate = ",round(error_rate,3))
    print("svm_classifier: finished")
    print(round(timeit.default_timer()-start,3), "seconds")


# load all data
file_data = openfile()
X_trn = file_data[0] # data/trn_img.npy
Y_trn = file_data[1] # data/trn_lbl.npy
X_dev = file_data[2] # data/dev_img.npy
Y_dev = file_data[3] # data/dev_lbl.npy
X_tst = file_data[4] # data/dev_lbl.npy

# question 1
bayes_classifier(X_trn, Y_trn, X_dev, Y_dev, True)

# question 2
acp_bayes_classifier(X_trn, Y_trn, X_dev, Y_dev)

# question 3
svm_classifier(X_trn, Y_trn, X_dev, Y_dev)
